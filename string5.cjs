function joinArray(arr) {
    if (arr.length === 0) {
        return '';
    }
    else {
        return arr.join(' ')+'.';
    }
}
module.exports = joinArray;