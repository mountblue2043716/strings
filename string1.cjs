function toNumber(item) {
    item = item.split("$").join("");
    item = item.split(",").join("");
    // if (parseFloat(item) === 'NaN') return 0;
    // if (typeof(item) !== "number") return 0;
    if (item !== parseFloat(item).toString()) return 0;
    return parseFloat(item);
} 

module.exports = toNumber;