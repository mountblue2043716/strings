function toTitleCase(word) {
    let str = ''
    for (let index = 0; index < word.length; index++) {
        if (index === 0) {
            str += word[index].toUpperCase()
        }
        else {
            str += word[index].toLowerCase()
        }
    }
    return str;
}
function getFullName(obj) {
    fullName = '';
    for (let name in obj) {
        fullName = fullName + toTitleCase(obj[name]) + ' ';
    }
    return fullName.slice(0, -1);
}
module.exports = getFullName;
